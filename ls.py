# Rumeet Goradia - rug5, 177008120
# Nihar Prabhala - np642, 179005552

import socket as mysoc
import sys
import threading
from multiprocessing import Process, Queue


def ts1Server(queue, hostname):
    try:
        ts1Socket = mysoc.socket(mysoc.AF_INET, mysoc.SOCK_STREAM)
    except mysoc.error as err:
        print('{} \n'.format("socket open error ", err))
        exit()

    # Define the port on which you want to connect to the server
    ts1IP = mysoc.gethostbyname(sys.argv[2])
    ts1Port = int(sys.argv[3])

    # connect to the server on local machine
    ts1ServerBinding = (ts1IP, ts1Port)

    try:
        ts1Socket.connect(ts1ServerBinding)
    except:
        print("Invalid TS1 port number or hostname.")
        exit()

    ts1Socket.send(hostname.encode('utf-8').strip())
    queue.put(ts1Socket.recv(250).decode('utf-8').strip())
    return


def ts2Server(queue, hostname):
    try:
        ts2Socket = mysoc.socket(mysoc.AF_INET, mysoc.SOCK_STREAM)
    except mysoc.error as err:
        print('{} \n'.format("socket open error ", err))
        exit()

    # Define the port on which you want to connect to the server
    ts2IP = mysoc.gethostbyname(sys.argv[4])
    ts2Port = int(sys.argv[5])

    # connect to the server on local machine
    ts2ServerBinding = (ts2IP, ts2Port)

    try:
        ts2Socket.connect(ts2ServerBinding)
    except:
        print("Invalid TS2 port number or hostname.")
        exit()

    ts2Socket.send(hostname.encode('utf-8'))
    queue.put(ts2Socket.recv(250).decode('utf-8'))
    return


if __name__ == '__main__':
    if (len(sys.argv) != 6):
        sys.exit("Improper number of arguments. Please try again with 5 arguments.")
    try:
        ss = mysoc.socket(mysoc.AF_INET, mysoc.SOCK_STREAM)
    except mysoc.error as err:
        print('{} \n'.format("socket open error ", err))
    lsListenPort = int(sys.argv[1])
    server_binding = ('', lsListenPort)
    ss.bind(server_binding)
    ss.listen(1)
    host = mysoc.gethostname()
    print("[S]: Server host name is: ", host)
    csockid, addr = ss.accept()
    print("[S]: Got a connection request from a client at", addr)

    # number of strings received from client
    numMessages = int(csockid.recv(1).decode('utf-8'))

    retMessage = ""
    for x in range(numMessages):
        hostName = csockid.recv(250).decode(
            'utf-8')
        queue = Queue()
        ts1Process = Process(target=ts1Server, args=(
            queue, hostName.lower().strip()))
        ts2Process = Process(target=ts2Server, args=(
            queue, hostName.lower().strip()))
        ts1Process.start()
        ts2Process.start()
        ts1Process.join(5)
        ts2Process.join(5)
        if (queue.empty()):
            retMessage = hostName + " - Error:HOST NOT FOUND"
        else:
            retMessage = queue.get()
        csockid.send(retMessage.strip().encode('utf-8'))

    # closing procedures
    queue = Queue()
    ts1Process = Process(target=ts1Server, args=(
        queue, "$END$"))
    ts2Process = Process(target=ts2Server, args=(
        queue, "$END$"))
    ts1Process.start()
    ts2Process.start()
    ts1Process.join()
    ts2Process.join()
    exit()


# lsThread = threading.Thread(name='lsServer', target=lsServer)
# lsThread.start()
