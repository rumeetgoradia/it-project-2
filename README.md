Rumeet Goradia - rug5, 177008120  
Nihar Prabhala - np642, 179005552

The code runs in Python 3.7.3.

1. _Briefly discuss how you implemented the LS functionality of tracking which TS responded to the query and timing out if neither TS responded._

   Upon receiving a hostname query from the client, the LS server sends the queried hostname to both TS servers. Each TS server process is given a maximum timeout of 5 seconds and is passed a queue. Then, if either TS server has a match for the queried hostname, it puts the appropriate response on the queue for the LS server to send back to the client. Else, if both TS servers timeout and nothing was added to the queue, the LS server will send an "not found" response back to the client.

2. _Are there known issues or functions that aren't working currently in your attached code? If so, explain._

   After extensive testing with all of the edge cases that we could think of, we did not determine that there are any issues with the functionality of our code.

3. _What problems did you face developing code for this project?_

    The main problem that was faced was how to incorporate parallel threads which each utilize a blocking recv() call and also set an appropriate timeout for each of the threads. We solved this problem using multiprocessing. This allowed us to use multiple processes at the same time and also set a timeout for them. 

    Another problem we faced was the communication between the TS server processes and the LS server's. The solution was to use a queue that was instantiated to empty by the LS server. This queue was then passed to both of the TS servers. If either of the servers had a hit, the response would be added to the queue. If the queue was empty, the LS server would know that neither TS server had a hit and would send a "not found" response.

4. _What did you learn by working on this project?_

    By working on this project we learned how to use multiprocessing to load balance. By splitting the queried hostnames among multiple servers, we were able to process the queries quicker while also having less throughput on the queried server.  