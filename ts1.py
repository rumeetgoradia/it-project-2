# Rumeet Goradia - rug5, 177008120
# Nihar Prabhala - np642, 179005552

import socket as mysoc
import sys
import threading


def ts1Server():
    try:
        ss = mysoc.socket(mysoc.AF_INET, mysoc.SOCK_STREAM)
    except mysoc.error as err:
        print('{}} \n'.format("socket open error ", err))
    ts1ListenPort = int(sys.argv[1])
    server_binding = ('', ts1ListenPort)
    ss.bind(server_binding)
    ss.listen(1)
    host = mysoc.gethostname()
    print("[TS1]: Server host name is: ", host)

    ts1Table = {}
    ts1File = open('./PROJ2-DNSTS1.txt', "r")

    # create dictionary from file
    for line in ts1File:
        line_split = line.split()
        ts1Table[line_split[0].lower()] = (
            line_split[0], line_split[1], line_split[2])

    print(ts1Table)

    queriedHostName = ""
    while(queriedHostName != "$END$"):
        csockid, addr = ss.accept()
        print("[TS1]: Got a connection request from a client at", addr)
        retMessage = ""
        queriedHostName = csockid.recv(250).decode('utf-8')
        print("[TS1]: Queried hostname is ", queriedHostName)
        if (queriedHostName != "$END$" and queriedHostName.lower().strip() in ts1Table):
            value = ts1Table.get(queriedHostName.lower().strip())
            hostName = value[0]
            ipAddress = value[1]
            flag = value[2]
            retMessage = hostName + " " + ipAddress + " " + flag
            csockid.send(retMessage.encode('utf-8'))
    ts1File.close()


if (len(sys.argv) != 2):
    sys.exit("Improper number of arguments. Please try again with 1 argument.")

ts1Thread = threading.Thread(name='ts1Server', target=ts1Server)
ts1Thread.start()
