# Rumeet Goradia - rug5, 177008120
# Nihar Prabhala - np642, 179005552

import socket as mysoc
import sys
import threading


def clientls():
    try:
        cs = mysoc.socket(mysoc.AF_INET, mysoc.SOCK_STREAM)
    except mysoc.error as err:
        print('{} \n'.format("socket open error ", err))

    # Define the port on which you want to connect to the server
    lsPort = int(sys.argv[2])
    lsIP = mysoc.gethostbyname(sys.argv[1])

    # connect to the server on local machine
    lsServerBinding = (lsIP, lsPort)
    try:
        cs.connect(lsServerBinding)
    except:
        print("Invalid LS port number or hostname.")
        exit()

    # open file with hostnames to be queried
    inputFile = open('./PROJ2-HNS.txt', "r")

    # send number of lines
    lineCount = len(inputFile.readlines())
    cs.send(str(lineCount).encode('utf-8'))

    # send hostnames to ls
    inputFile.seek(0)
    tsHost = ""

    outputStr = ""

    for queriedHostname in inputFile:
        cs.send(queriedHostname.strip().encode('utf-8'))

        # receive and use ls's response
        lsResponse = cs.recv(250).decode('utf-8')
        lsResponseSplit = lsResponse.split(" ")
        outputStr = outputStr + lsResponse + "\n"

# write output
    outputFile = open("./RESOLVED.txt", "w")
    output = outputStr.strip()
    outputFile.write(output)

# closing procedures
    outputFile.close()
    inputFile.close()
    cs.close()
    exit()


if (len(sys.argv) != 3):
    sys.exit("Improper number of arguments. Please try again with 2 arguments.")

lsThread = threading.Thread(name='clientls', target=clientls)  # ls thread
lsThread.start()

exit()
